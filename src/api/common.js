/* eslint-disable no-restricted-globals */

// get param direct from process.env, because of bundle param
export const SERVER_PORT = process.env.SERVER_PORT || 8000;

// can use same hostname as web version
export const SERVER_HOST =
    process.env.SERVER_HOST || location.hostname || 'localhost';

export const { origin: SERVER_BASE } = new URL(
    `http://${SERVER_HOST}:${SERVER_PORT}`
);

export const API_BASE = `${SERVER_BASE}/api`;

export const WS_BASE = `ws://${SERVER_HOST}:${SERVER_PORT}`;

export const rejectErrors = res => {
    const { status } = res;
    if (status >= 200 && status < 300) {
        return res;
    }
    // we can get message from Promise but no need, just use statusText instead of
    // server return errors, also status code
    return Promise.reject(new Error(res.statusText));
};

export class RingBuffer {
    constructor(length, onClean) {
        this.buffer = new Array(length);
        this.pointer = 0;
        this.onClean = onClean;
    }

    get(i) {
        return this.buffer[i];
    }

    push(item) {
        if (this.onClean && this.buffer[this.pointer]) {
            this.onClean(this.buffer[this.pointer]);
        }
        this.buffer[this.pointer] = item;
        this.pointer++;
        if (this.pointer === this.buffer.length) {
            this.pointer = 0;
        }
    }

    clean() {
        if (this.onClean) {
            for (const i in this.buffer) {
                if (this.buffer[i]) {
                    // do job for item, and delete it so we do not re-clean
                    this.onClean(this.buffer[i]);
                    delete this.buffer[i];
                }
            }
        }
    }
}