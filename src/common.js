export const getSnapshot = (canvas, quality = 1, contentType = 'image/jpeg') =>
    new Promise(resolve => {
        canvas.toBlob(resolve, contentType, quality);
    });

export const copyVideo2Canvas = (video, canvas) => {
    canvas.width = video.videoWidth;
    canvas.height = video.videoHeight;
    canvas.getContext('2d').drawImage(video, 0, 0, canvas.width, canvas.height);
};