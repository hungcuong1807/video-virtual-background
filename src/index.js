import React from 'react';
import ReactDOM from 'react-dom';
import './bootstrap.min.css';
import * as bodyPix from '@tensorflow-models/body-pix';
import * as tf from "@tensorflow/tfjs";
import { getSnapshot, copyVideo2Canvas } from "./common"
import './style.css';

import { Spin } from "antd"
import { fetchWithOptions } from "./api"

class Video extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      net: null,
      file: null,
      isLoading: false,
      style: {
        display: 'block',
      },
      styleResult: {
        display: 'none'
      },
      images: null
    }
    this.videoTag = React.createRef();
    this.canvasTag = React.createRef();
    this.detectBody = this.detectBody.bind(this);
    this.canvasVideo = React.createRef();


    bodyPix.load({
      architecture: 'MobileNetV1',
      outputStride: 16,
      multiplier: 0.75,
      quantBytes: 2
    })
      .catch(error => {
        console.log(error);
      })
      .then(objNet => {
        {
          this.net = objNet
        }
      });
  }

  componentDidMount() {
    // getting access to webcam
    navigator.mediaDevices
      .getUserMedia({ video: true })
      .then(stream => {

        this.videoTag.current.srcObject = stream
        this.videoTag.current.play()
      })
      .catch((error) => { console.log(error) });
  }


  detectBody() {
    if (this.net != null) {
      this.net.segmentPerson(this.videoTag.current, {
        flipHorizontal: true,
        internalResolution: 'low',
        segmentationThreshold: 0.8,
        maxDetections: 1
      }).then(personsegmentation => {
        const mask = this.toMask(personsegmentation);
        bodyPix.drawMask(this.canvasTag.current, this.videoTag.current, mask, 1, 0, true);
      })
        .catch(err => { console.log(err) });

    }

    requestAnimationFrame(this.detectBody);

  }
  handleChangeBackground = (e) => {

    let file = e.target.files[0];
    this.setState({ file: file, style: { display: 'none' } })
    var reader = new FileReader();
    reader.readAsDataURL(file);
    const height = this.props.height;
    const width = this.props.width;
    reader.onloadend = (e) => {
      var image = new Image();
      image.src = e.target.result;
      image.onload = (ev) => {
        this.canvasTag.current.getContext('2d').drawImage(image, 0, 0, width, height);
        const ctx = this.canvasTag.current.getContext('2d')
        this.videoTag.current.play();
        this.imageData = this.canvasTag.current.getContext("2d").getImageData(0, 0, width, height);
        this.detectBody();
      }
    }
  }
  toMask = (personOrPartSegmentation) => {
    if (
      Array.isArray(personOrPartSegmentation) &&
      personOrPartSegmentation.length === 0
    ) {
      return null;
    }
    var multiPersonOrPartSegmentation;
    if (!Array.isArray(personOrPartSegmentation)) {
      multiPersonOrPartSegmentation = [personOrPartSegmentation];
    } else {
      multiPersonOrPartSegmentation = personOrPartSegmentation;
    }
    var width = multiPersonOrPartSegmentation[0].width;
    var height = multiPersonOrPartSegmentation[0].height;

    var bytes = new Uint8ClampedArray(this.imageData.data);
    for (var i = 0; i < height; i += 1) {
      for (var j = 0; j < width; j += 1) {
        var n = i * width + j;
        for (var k = 0; k < multiPersonOrPartSegmentation.length; k++) {
          if (multiPersonOrPartSegmentation[k].data[n] === 1) {
            bytes[4 * n] = 0;
            bytes[4 * n + 1] = 0;
            bytes[4 * n + 2] = 0;
            bytes[4 * n + 3] = 0;
          }
        }
      }
    }
    return new ImageData(bytes, width, height);
  }


  // sendImageToSegment = async () => {
  //   const detectQuality = 0.9
  //   if (this.videoTag && this.videoTag.current) {
  //     copyVideo2Canvas(this.videoTag.current, this.canvasVideo.current)
  //     const snapshot = await getSnapshot(this.canvasVideo.current, detectQuality)
  //     this.videoTag.current.pause();
  //     if (snapshot && this.state.file) {
  //       this.setState({ isLoading: true })
  //       await this.processSnapshot(snapshot);
  //     }
  //   }
  // }
  // processSnapshot = async (snapshot) => {
  //   const formData = new FormData();
  //   formData.append("data", snapshot);
  //   formData.append("bg", this.state.file);
  //   const controller = new AbortController();
  //   const signal = controller.signal;
  //   setTimeout(() => controller.abort(), 10000);

  //   const ret = await fetchWithOptions("/segmentation", {
  //     method: 'POST',
  //     body: formData,
  //     signal
  //   });
  //   this.setState({ isLoading: false, styleResult: { display: 'block' }, images: `data:image/png;base64,${ret.img}` })
  // };

  // download = (e) => {
  //   const dataUrl = this.canvasTag.current.toDataURL()
  //   e.target.href = dataUrl
  // }
  // drawBody(personSegmentation) {
  //   this.canvasTag.current.getContext('2d').drawImage(this.videoTag.current, 0, 0, this.props.width, this.props.height);
  //   var imageData = this.canvasTag.current.getContext('2d').getImageData(0, 0, this.props.width, this.props.height);
  //   var pixel = imageData.data;
  //   for (var p = 0; p < pixel.length; p += 4) {
  //     if (personSegmentation.data[p / 4] == 0) {
  //       pixel[p + 3] = 0;
  //     }
  //   }
  //   this.canvasTag.current.getContext('2d').imageSmoothingEnabled = true;
  //   this.canvasTag.current.getContext('2d').putImageData(imageData, 0, 0);
  // }





  render() {
    return <div>
      <Spin spinning={this.state.isLoading}>
        <video id={this.props.id}
          ref={this.videoTag}

          width={this.props.width}
          height={this.props.height}
          title={this.props.title}
          style={this.state.style}>
        </video>

        <canvas className="person" ref={this.canvasTag} width={this.props.width}
          height={this.props.height}></canvas>
        {/* <canvas ref={this.canvasVideo} style={{ display: 'none' }} width={this.props.width}
          height={this.props.height}></canvas>
        <img style={this.state.styleResult} src={this.state.images} /> */}


        {/* <a className="download-link" href="download" download={"mycanvas.png"} onClick={this.sendImageToSegment}>
          Take a photo
      </a> */}


        <div style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
          <input type="file" onChange={this.handleChangeBackground} />
        </div>

        <button onClick={this.sendImageToSegment}>Click to send video</button>

      </Spin>

    </div >
  }
}




class App extends React.Component {
  constructor(props) {
    super(props);
  }


  render() {

    return (
      <div>
        <Video height={468} width={640} />
      </div>

    )
  }

}

ReactDOM.render(
  <App />,
  document.getElementById('root')
);


